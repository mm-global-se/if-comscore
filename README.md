# comScore

---

[TOC]

## Overview

This integration is used for extracting campaign experience data from the Maxymiser platform and sending it to __comScore__.

## How we send the data

We define `udm_()` function according to integration documentation provided by __comScore__ and then use it for sending campaign data.

`udm_()` function takes URL as an argument. That URL contains data such as account data, campaign name and experience. `ns_m_exp` URL parameter contains campaign name (e.g `MM_Prod_T1Button`) and `ns_m_chs` parameter contains campaign experience (e.g. `color:red`)

## Data Format

The data sent to comScore will be in the following format:

+ Campaign Name: `<mode>_<campaign name>`

+ Campaign Experience: `<element1>:<variant>|<element2>:<variant>`

Please find additional information on the format of the sent data [here](https://bitbucket.org/mm-global-se/integration-factory#markdown-header-introduction
)

## Prerequisite

+ Campaign name

+ `account` (ComScore account ID)

+ `ns_site` (optional parameter)

## Download

* [comscore-register.js](https://bitbucket.org/mm-global-se/if-comscore/src/master/src/comscore-register.js)

* [comscore-initialize.js](https://bitbucket.org/mm-global-se/if-comscore/src/master/src/comscore-initialize.js)

## Deployment instructions

### Content Campaign

+ Ensure that you have the Integration Factory plugin deployed on site level([find out more](https://bitbucket.org/mm-global-se/integration-factory)). Map this script to the whole site with an _output order: -10_ 

+ Create another site script and add the [comscore-register.js](https://bitbucket.org/mm-global-se/if-comscore/src/master/src/comscore-register.js) script.  Map this script to the whole site with an _output order: -5_

    __NOTE!__ Please check whether the integration already exists on site level. Do not duplicate the Integration Factory plugin and the Google Analytics Register scripts!

+ Create a campaign script and add the [comscore-initialize.js](https://bitbucket.org/mm-global-se/if-comscore/src/master/src/comscore-initialize.js) script. Customise the code by changing the campaign name, account ID (only if needed), and slot number accordingly and add to campaign. Map this script to the page where the variants are generated on, with an _output order: 0_

### Redirect Campaign

For redirect campaigns, the campaign script needs to be mapped to both the generation and redirected page. To achieve this follow the steps bellow:

+ Go through the instructions outlined in [Content Campaign](https://bitbucket.org/mm-global-se/if-ga#markdown-header-content-campaign)

+ Make sure that in the initialize campaign script, redirect is set to true.

+ Create and map an additional page where the redirect variant would land on. 

+ Map the integration initialize campaign script to this page as well.