mmcore.IntegrationFactory.register('comScore', {
  defaults: {},

  validate: function (data) {
    if(!data.campaign)
        return 'No campaign.';

    if(!data.account)
        return 'No account ID provided.';

    return true;
  },

  check: function (data) {
    return true;
  },

  exec: function (data) {
    var mode = data.isProduction ? 'MM_Prod_' : 'MM_Sand_',
        name = mode + data.campaign,
        experience = data.campaignInfo.replace(data.campaign + '=', '');

    function udm_(a){var j,m,n,o,p,b="comScore=",c=document,d=c.cookie,e="",f="indexOf",g="substring",h="length",i=2048,k="&ns_",l="&",q=window,r=q.encodeURIComponent||escape;if(d[f](b)+1)for(o=0,n=d.split(";"),p=n[h];p>o;o++)m=n[o][f](b),m+1&&(e=l+unescape(n[o][g](m+b[h])));a+=k+"_t="+ +new Date+k+"c="+(c.characterSet||c.defaultCharset||"")+"&c8="+r(c.title)+e+"&c7="+r(c.URL)+"&c9="+r(c.referrer),a[h]>i&&a[f](l)>0&&(j=a[g](0,i-8).lastIndexOf(l),a=(a[g](0,j)+k+"cut="+r(a[g](j+1)))[g](0,i)),c.images?(m=new Image,q.ns_p||(ns_p=m),m.src=a):c.write("<","p","><",'img src="',a,'" height="1" width="1" alt="*"',"><","/p",">")}

    udm_(
      'http'
        + (document.location.href.charAt(4) == 's' ? 's://sb' : '://b')
        + '.scorecardresearch.com/b?c1=2'
        + '&c2=' + data.account
        + (data.ns_site ? '&ns_site=' + data.ns_site : '')
        + '&ns_m_exp=' + name
        + '&ns_m_chs=' + experience);

    if (typeof data.callback === 'function') data.callback();
    return true;
  }
});
